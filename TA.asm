.MODEL SMALL
.CODE
ORG 100h
   
GASS:    
    MOV CL,30H                                    
    JMP PROSES
    CTH0 DB 'Ketikkan satu kalimat:$'
    CTH1 DB 13,10,'Kalimat yang diinput adalah $'
    CTH2 DB 13,10,'Cetak dari belakang :$',13,10  
    CTHX DB 13,10,'$'
    CTHZ DB 20,?,20 DUP (?)                   
    
    
VOKAL:  INC CL                    
        JMP PLUS                        
        

                          
PROSES:            
    ;CETAK STRING
    
    MOV AH,09H
    MOV DX,OFFSET CTH0
    INT 21H  
    
    
    ;INPUT STRING
    
    MOV AH,0AH
    LEA DX,CTHZ
    INT 21H
    
    ;CETAK STRING
    
    MOV AH,09H
    MOV DX,OFFSET CTH1
    INT 21H
    
    ;CETAK VARIABEL STRING DG MODEL COMPARASI
    
    MOV BX,OFFSET CTHZ+2        
    
    
CETAK:
    MOV DL,[BX]                    
    CMP DL,0DH                    
    JE BELAKANG                    
        
    MOV AH,02H
    INT 21H
    
        CMP DL,'a'
        JE VOKAL   
        CMP DL,'i'
        JE VOKAL
        CMP DL,'u'
        JE VOKAL
        CMP DL,'e'
        JE VOKAL
        CMP DL,'o'
        JE VOKAL
        CMP DL,'A'
        JE VOKAL
        CMP DL,'I'
        JE VOKAL
        CMP DL,'U'
        JE VOKAL
        CMP DL,'E'
        JE VOKAL
        CMP DL,'O'
        JE VOKAL 
        
PLUS:INC BX
     JMP CETAK    
   
    ;CETAK KATA DARI BELAKANG

BELAKANG:MOV AH,09H
         MOV DX,OFFSET CTH2
         INT 21H   
         MOV AH,09H
         MOV DX,OFFSET CTHX            
         INT 21H                     

CETAK1:    
    MOV DL,[BX]                    
    CMP DL,CTHZ+1                
    JE SELESAI                   
                     
    MOV AH,02H    
    INT 21H
    DEC BX
    JMP CETAK1
      
 
            
SELESAI:
    INT 20H
    
    END GASS
